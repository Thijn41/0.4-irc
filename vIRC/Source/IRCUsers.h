#pragma once

#include "IRCServerInfo.h"
#include "RefCounter.h"

class CIRCUser : public IRefCounter
{
public:
	CIRCUser( CIRCServerInfo* pServerInfo, const char* szName );
	virtual ~CIRCUser( void );

	const char*						GetName						( void ) const						{ return m_szName; }
	void							SetName						( const char* sz )					{ irc_strcpy( m_szName, sz, m_pServerInfo->GetMaxNickLen() ); }

private:
	char*							m_szName;

	CIRCServerInfo*					m_pServerInfo;
};

class CIRCUsers
{
public:
	CIRCUsers( CIRCServerInfo* pServerInfo );
	~CIRCUsers(void);

	CIRCUser*						New							( const char* szName );

	CIRCUser*						Find						( const char* szName )				{ return m_pUsers[ std::string( szName ) ]; }

	bool							Remove						( CIRCUser* p );
	bool							Remove						( const char* szName );

	void							RemoveAll					( void );

	unsigned int					Count						( void ) const						{ return m_uiCount; }

private:
	unsigned int					m_uiCount;
	stdext::hash_map< std::string, CIRCUser* > m_pUsers;
	CIRCServerInfo*					m_pServerInfo;
};
