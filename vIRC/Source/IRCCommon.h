#pragma once

#include <string>
#include <string.h>

#ifdef _WIN32
#include <hash_map>

#define irc_strtok_s strtok_s
#else
#include <ext/hash_map>
namespace stdext = __gnu_cxx;

namespace __gnu_cxx
{ 
	template<> struct hash< std::string > 
	{ 
		size_t operator()( const std::string& x ) const 
		{ 
			return hash< const char* >()( x.c_str() ); 
		} 
	}; 
} 

#define irc_strtok_s strtok_r
#endif

bool irc_isequal( const char* s1, const char* s2 );
void irc_strcpy( char* szDest, const char* szSource, size_t size );
void irc_strcat( char* szDest, const char* szSource, size_t size );

size_t NumTok( const char* szText, const char cDelimiter );
bool IsNum( const char* sz );
