#pragma once

#include "IRCUsers.h"

struct IRC_TOPIC_INFO
{
	char* szTopic;
	char* szNick;
	time_t uiTime;
};

struct IRC_CHANNEL_USERS
{
	IRC_CHANNEL_USERS* pPrev;
	char cModes[ 12 ];
	CIRCUser* pUser;
	IRC_CHANNEL_USERS* pNext;
};

class CIRCChannel
{
public:
	CIRCChannel( CIRCServerInfo* pServerInfo, CIRCUsers* pUsers, const char* szName );
	~CIRCChannel( void );

	const char*						GetName						( void ) const						{ return m_szName; }

	void							SetTopic					( const char* szTopic )				{ strcpy( m_pTopicInfo.szTopic, szTopic ); }
	const char*						GetTopic					( void ) const						{ return m_pTopicInfo.szTopic; }

	void							SetTopicSetter				( const char* szNick )				{ strcpy( m_pTopicInfo.szNick, szNick ); }
	const char*						GetTopicSetter				( void ) const						{ return m_pTopicInfo.szNick; }

	void							SetTopicSetTime				( const time_t& ui )				{ m_pTopicInfo.uiTime = ui; }
	const time_t					GetTopicSetTime				( void ) const						{ return m_pTopicInfo.uiTime; }

	void							SetChannelModes				( const char* sz );
	const char*						GetChannelModes				( void ) const						{ return m_cChannelModes; }

	void							AddUser						( char* sz );
	void							RemoveUser					( const char* sz );
	void							RemoveAllUsers				( void );

	const char*						GetUserModes				( const char *sz );

	size_t							CountUsers					( void ) const						
	{ 
		if ( m_pUsers )
			return m_pUsers->Count(); 
		return 0;
	}

private:
	char*							m_szName;

	IRC_TOPIC_INFO					m_pTopicInfo;

	char							m_cChannelModes[ 32 ];

	IRC_CHANNEL_USERS*				m_pChannelUsers;

	// Used to determine limits etc
	CIRCServerInfo*					m_pServerInfo;
	CIRCUsers*						m_pUsers;
};

class CIRCChannels
{
public:
	CIRCChannels( CIRCServerInfo* pServerInfo, CIRCUsers* pUsers );
	~CIRCChannels(void);

	CIRCChannel*					New							( const char* szName );

	CIRCChannel*					Find						( const char* szName )				
	{ 
		return m_pChannels[ std::string( szName ) ]; 
	}

	bool							Remove						( CIRCChannel* p );
	bool							Remove						( const char* szName );

	void							RemoveAll					( void );

	unsigned int					Count						( void ) const						{ return m_uiCount; }

	stdext::hash_map< std::string, CIRCChannel* >	GetList		( void ) const						{ return m_pChannels; }

private:
	unsigned int					m_uiCount;
	stdext::hash_map< std::string, CIRCChannel* > m_pChannels;
	CIRCServerInfo*					m_pServerInfo;
	CIRCUsers*						m_pUsers;
};
