#include "IRCServerInfo.h"

CIRCServerInfo::CIRCServerInfo(void)
: m_szNetworkName( 0 ), m_uiMaxNickLen( 32 ), m_uiMaxChanNameLen( 32 ), m_uiMaxChanTopicLen( 307 ), m_uiMaxChanModes( 12 ), m_uiMaxChannels( 100 ), 
m_uiMaxKickReasonLen( 307 ), m_uiMaxAwayReasonLen( 307 ), m_bExcepts( false ), m_bInvex( false )
{
}

CIRCServerInfo::~CIRCServerInfo(void)
{
	if ( m_szNetworkName )
	{
		delete [] m_szNetworkName;
		m_szNetworkName = 0;
	}
}

void CIRCServerInfo::ParseISupportMode( const char* sz, const size_t uiTokens )
{
	char szTmp[ 160 ] = { 0 };
	char szParam[ 32 ] = { 0 };
	char szValue[ 128 ] = { 0 };
	irc_strcpy( szTmp, sz, sizeof( szTmp ) );
	if ( uiTokens )
	{
		irc_strcpy( szParam, strtok( szTmp, "=" ), sizeof( szParam ) );
		if ( uiTokens > 1 ) irc_strcpy( szValue, strtok( 0, " " ), sizeof( szValue ) );

		switch( szParam[ 0 ] )
		{
		case 'A':
			{
				if ( irc_isequal( szParam, "AWAYLEN" ) )
				{
					m_uiMaxAwayReasonLen = (unsigned int)atoi( szValue );
				}
				break;
			}

		case 'C':
			{
				if ( irc_isequal( szParam, "CHANNELLEN" ) )
				{
					m_uiMaxChanNameLen = (unsigned int)atoi( szValue );
				}
				break;
			}

		case 'E':
			{
				if ( irc_isequal( szParam, "EXCEPTS" ) )
					m_bExcepts = true;

				break;
			}

		case 'I':
			{
				if ( irc_isequal( szParam, "INVEX" ) )
					m_bInvex = true;

				break;
			}

		case 'K':
			{
				if ( irc_isequal( szParam, "KICKLEN" ) )
				{
					m_uiMaxKickReasonLen = (unsigned int)atoi( szValue );
				}
				break;
			}

		case 'M':
			{
				if ( irc_isequal( szParam, "MAXCHANNELS" ) )
				{
					m_uiMaxChannels = (unsigned int)atoi( szValue );
				}
				break;
			}

		case 'N':
			{
				if ( irc_isequal( szParam, "NETWORK" ) )
				{
					size_t uiLen = strlen( szValue ) + 1;
					// If memory is already allocated, delete it
					if ( m_szNetworkName )
					{
						delete [] m_szNetworkName;
						m_szNetworkName = 0;
					}

					m_szNetworkName = new char [ uiLen ];

					irc_strcpy( m_szNetworkName, szValue, uiLen );
				}
				else if ( irc_isequal( szParam, "NICKLEN" ) )
				{
					m_uiMaxNickLen = (unsigned int)atoi( szValue );
				}
				break;
			}

		case 'P':
			{
				if ( irc_isequal( szParam, "PREFIX" ) )
				{
					char* p = szValue;

					// Skip the (
					*p++;

					size_t ui = 0;
					while ( *p != ')' )
					{
						m_sPrefixModes[ ui ].cMode = *p++;

						++ui;
					}

					// Skip the )
					*p++;

					ui = 0;
					while ( *p )
					{
						m_sPrefixModes[ ui ].cPrefix = *p++;

						++ui;
					}
				}
				break;
			}

		case 'T':
			{
				if ( irc_isequal( szParam, "TOPICLEN" ) )
				{
					m_uiMaxChanTopicLen = (unsigned int)atoi( szValue );
				}
				break;
			}
		}
	}
}

